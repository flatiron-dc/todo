function createNewTask(event) {
  let newTask = document.createElement('li');
  let newItemTextValue = document.getElementById("new_item_text").value
  newTask.innerHTML = "<input type='checkbox' class='remove-button'>" + newItemTextValue
  document.getElementById("list").appendChild(newTask);
  document.getElementById("new_item_text").value = ''
}

function onEnter(event) {
  if(event.code === 'Enter') {
    createNewTask(event)
  }
}

function remove(event) {
  console.log(event)
}


document.getElementById("new_item_text").addEventListener('keydown', onEnter)
document.getElementById("create_new_task_button").addEventListener("click", createNewTask);
document.querySelector('body').addEventListener('click', function(event) {
  if (event.target.tagName.toLowerCase() === 'input') {
    remove(event)
    // do your action on your 'li' or whatever it is you're listening for
  }
});
